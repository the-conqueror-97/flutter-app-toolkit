# Media File Copy and Rename Script

This PowerShell script automates the process of copying and renaming media files (e.g., PNG, JPG, MP4) based on specific patterns. It renames files to lowercase, prefixes them with "ic_", and copies them to corresponding target directories based on their resolution suffix.

## Prerequisites

- Windows PowerShell installed on your system.
- Basic understanding of PowerShell scripting.

## Usage

1. **Download the Script**:
   - Download the `copyIconAssets.ps1` script from this repository.

2. **Prepare Your Directories**:
   - Ensure you have a source directory (`mediaDir`) containing your media files (e.g., images, videos).
   - Prepare a target directory (`targetDir`) where you want the files to be copied.

3. **Run the Script**:
   - Open PowerShell.
   - Navigate to the directory where `copyIconAssets.ps1` is located.

4. **Execute the Script**:
   - Run the script by executing the following command:
     ```powershell
     ./copyIconAssets.ps1 -targetDir "Path\to\your\project\assets" -mediaDir "Path\to\your\media\directory"
     ```
     - Replace `"Path\to\your\project\assets"` with the path to your target directory.
     - Replace `"Path\to\your\media\directory"` with the path to your media directory.

5. **Follow Prompts (if necessary)**:
   - If you do not provide the `-targetDir` or `-mediaDir` parameters, the script will prompt you to enter them.

6. **Script Execution**:
   - The script will start processing the media files.
   - It will rename each file to lowercase, prefix it with "ic_", and copy it to the appropriate target subdirectory based on its resolution suffix (e.g., 24px, 36px).

7. **Completion**:
   - Once the script finishes, check your target directory to verify that the media files have been copied and renamed correctly.

## Notes

- The script supports various media file types, including PNG, JPG, JPEG, GIF, MP4, MOV, AVI, MKV.
- Make sure to review and customize the script according to your specific file naming patterns and directory structures if necessary.

## Author

- Fatih Gucuko

