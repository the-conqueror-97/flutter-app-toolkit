<#
.SYNOPSIS
    Copy and rename media files based on specific patterns.
.DESCRIPTION
    This script collects all media files (e.g., PNG, JPG, MP4) from a specified images directory, checks if the files end with specific patterns 
    (24px, 36px, 48px, 72px, 96px), renames them to lowercase, prefixes them with "ic_", and copies them to corresponding target subdirectories.
.AUTHOR
    Fatih Gucuko
#>

param (
    [string]$targetDir,
    [string]$mediaDir
)

# Prompt for target directory if not provided
if (-not $targetDir) {
    $targetDir = Read-Host "Please enter the target directory"
}

# Prompt for media directory if not provided
if (-not $mediaDir) {
    $mediaDir = Read-Host "Please enter the media directory"
}

# Echo script information
Write-Host "Script: Copy and Rename Media Files" -ForegroundColor Green
Write-Host "Author: Fatih Gucuko" -ForegroundColor Green
Write-Host "Description: This script copies and renames media files from the media directory to the target directory based on specific patterns." -ForegroundColor Green

# Define target subdirectories
$target24px = "$targetDir"
$target32px = "$targetDir\1.33x"
$target36px = "$targetDir\1.5x"
$target48px = "$targetDir\2.0x"
$target64px = "$targetDir\2.67x"
$target72px = "$targetDir\3.0x"
$target96px = "$targetDir\4.0x"
$target128px = "$targetDir\5.33x"

# Create target directories if they do not exist
$dirs = @($target24px, $target32px, $target36px, $target48px, $target64px, $target72px, $target96px, $target128px)
foreach ($dir in $dirs) {
    if (-Not (Test-Path $dir)) {
        New-Item -ItemType Directory -Path $dir
    }
}

# Get all media files from the media directory
$mediaFiles = Get-ChildItem -Path $mediaDir -Include *.png, *.jpg, *.jpeg, *.gif, *.mp4, *.mov, *.avi, *.mkv -Recurse

foreach ($file in $mediaFiles) {
    # Check the file name for specific patterns and copy accordingly
    if ($file.Name -match "(.+)_24px\.(.+)$") {
        $newFileName = "$($matches[1]).$($matches[2])".ToLower() # Rename file to lowercase and prefix with "ic_"
        if ($newFileName.StartsWith("ic_") -eq $false) {
            $newFileName = "ic_$newFileName"
        }
        Copy-Item -Path $file.FullName -Destination (Join-Path -Path $target24px -ChildPath $newFileName) # Copy file to target directory
    }
    elseif ($file.Name -match "(.+)_32px\.(.+)$") {
        $newFileName = "$($matches[1]).$($matches[2])".ToLower() # Rename file to lowercase and prefix with "ic_"
        if ($newFileName.StartsWith("ic_") -eq $false) {
            $newFileName = "ic_$newFileName"
        }
        Copy-Item -Path $file.FullName -Destination (Join-Path -Path $target32px -ChildPath $newFileName) # Copy file to target directory
    }
    elseif ($file.Name -match "(.+)_36px\.(.+)$") {
        $newFileName = "$($matches[1]).$($matches[2])".ToLower() # Rename file to lowercase and prefix with "ic_"
        if ($newFileName.StartsWith("ic_") -eq $false) {
            $newFileName = "ic_$newFileName"
        }
        Copy-Item -Path $file.FullName -Destination (Join-Path -Path $target36px -ChildPath $newFileName) # Copy file to target directory
    }
    elseif ($file.Name -match "(.+)_48px\.(.+)$") {
        $newFileName = "$($matches[1]).$($matches[2])".ToLower() # Rename file to lowercase and prefix with "ic_"
        if ($newFileName.StartsWith("ic_") -eq $false) {
            $newFileName = "ic_$newFileName"
        }
        Copy-Item -Path $file.FullName -Destination (Join-Path -Path $target48px -ChildPath $newFileName) # Copy file to target directory
    }
    elseif ($file.Name -match "(.+)_64px\.(.+)$") {
        $newFileName = "$($matches[1]).$($matches[2])".ToLower() # Rename file to lowercase and prefix with "ic_"
        if ($newFileName.StartsWith("ic_") -eq $false) {
            $newFileName = "ic_$newFileName"
        }
        Copy-Item -Path $file.FullName -Destination (Join-Path -Path $target64px -ChildPath $newFileName) # Copy file to target directory
    }
    elseif ($file.Name -match "(.+)_72px\.(.+)$") {
        $newFileName = "$($matches[1]).$($matches[2])".ToLower() # Rename file to lowercase and prefix with "ic_"
        if ($newFileName.StartsWith("ic_") -eq $false) {
            $newFileName = "ic_$newFileName"
        }
        Copy-Item -Path $file.FullName -Destination (Join-Path -Path $target72px -ChildPath $newFileName) # Copy file to target directory
    }
    elseif ($file.Name -match "(.+)_96px\.(.+)$") {
        $newFileName = "$($matches[1]).$($matches[2])".ToLower() # Rename file to lowercase and prefix with "ic_"
        if ($newFileName.StartsWith("ic_") -eq $false) {
            $newFileName = "ic_$newFileName"
        }
        Copy-Item -Path $file.FullName -Destination (Join-Path -Path $target96px -ChildPath $newFileName) # Copy file to target directory
    }
    elseif ($file.Name -match "(.+)_128px\.(.+)$") {
        $newFileName = "$($matches[1]).$($matches[2])".ToLower() # Rename file to lowercase and prefix with "ic_"
        if ($newFileName.StartsWith("ic_") -eq $false) {
            $newFileName = "ic_$newFileName"
        }
        Copy-Item -Path $file.FullName -Destination (Join-Path -Path $target128px -ChildPath $newFileName) # Copy file to target directory
    }
}
